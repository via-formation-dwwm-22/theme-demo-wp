<?php
get_header(); //affiche le header ?>
    <main>
        <?php if (have_posts()) { ?>
            <section>
                <h1>Voici tous nos restaurants</h1>
                <div class="restaurants-container">
                    <?php while (have_posts()) {
                        the_post(); ?>
                        <a href="<?= get_the_permalink(); //affiche l'url du restaurant  ?>">
                            <article>
                                <?php the_post_thumbnail('large'); //affiche l'image mise en avant ?>
                                <h2>
                                    <?php the_title(); //affiche le titre, équivalent à echo get_the_title();?>
                                </h2>
                                <p>
                                    <?php the_excerpt(); //affiche l'extrait de publication ?>
                                </p>
                            </article>
                        </a>
                    <?php } ?>
                </div>
            </section>
        <?php } ?>
    </main>
<?php get_footer(); //affiche le footer
