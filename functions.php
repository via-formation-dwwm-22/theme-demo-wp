<?php
//ajoute des fichiers de script aux pages du front
add_action("wp_enqueue_scripts", "christine_script");
function christine_script(){
    wp_enqueue_script("christinejs", get_stylesheet_directory_uri()."/script.js", [], 0.1, true);
    wp_enqueue_script("bootstrapjs", "https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js");
}

//ajoute des fichiers de style aux page du front
add_action("wp_enqueue_scripts", "christine_style");
function christine_style(){
    wp_enqueue_style("bootstrapcss", "https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css", [], 5.1 );
}

//crée un n ouveau type de contenu personalisé (CPT)
add_action("init", "christine_cpt");
function christine_cpt(){
    $args = [
        "label" => "Recette", // nom dansd le menu en back-office
        "labels"    => [
          "name"    => "Recette",
        ], //noms personalisés dans l'affichage en back-office
        "public"    => true, //est visible dans le back-office et le front
        "hierarchical"  => false, //se comporte comme un article et non comme une page
        "exclude_from_search"   => false, //les recettes apparaitereont dans les résulats de recherche
        "show_in_rest"  => true, //permet d'afficher l'éditeur Gutenberg (celui en bloc)
        "menu_position" => 5,
        "menu_icon" => "dashicons-buddicons-community",
        "supports"  => ['title', 'editor', 'comments', 'revisions', 'trackbacks', 'author', 'excerpt', 'page-attributes', 'thumbnail', 'custom-fields', 'post-formats'],
        "has_archive"   => true,
    ];
    register_post_type("recette", $args);
}
