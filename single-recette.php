<?php
get_header(); ?>
<main>
    <div class="recette-container">
        <?php if (have_posts()){
            while (have_posts()){
                the_post(); ?>
                <div>
                <h1><?php the_title(); ?></h1>

                    <p>Ingrédients: </p>
                <?php echo get_field('ingredients'); ?>

                <p>Note de la recette : <?php the_field('rating'); //affiche le champs personnalisé rating ?> </p>
                    <?php the_content(); //affiche le contenu des blocs gutenberg ?>
                </div>
                <div>
                    <figure><?php the_post_thumbnail();?></figure>
                </div>
            <?php }
        } ?>
    </div>
</main>
<?php get_footer();
